package ma.octo.assignement.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Role;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

	@Autowired
	private VirementRepository virementRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	Virement virementSaved;
	Compte compteEmetteur;
	Compte compteBeneficiaire;

	@BeforeEach
	void setUp() throws Exception {
		Role roleAdmin = new Role();
		roleAdmin.setName("admin");
		roleRepository.save(roleAdmin);
		
		Utilisateur admin = new Utilisateur();
		admin.setUserName("admin");
		admin.setFirstName("MahdiAdmin");
		admin.setLastName("BouhouchAdmin");
		admin.setGender("Male");
		admin.setRole(roleRepository.getById(1L));
		admin.setEncryptedPassword(new BCryptPasswordEncoder().encode("123"));
		
		utilisateurRepository.save(admin);
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUserName("user1");
		utilisateur1.setLastName("last1");
		utilisateur1.setFirstName("first1");
		utilisateur1.setGender("Male");

		utilisateurRepository.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUserName("user2");
		utilisateur2.setLastName("last2");
		utilisateur2.setFirstName("first2");
		utilisateur2.setGender("Female");

		utilisateurRepository.save(utilisateur2);

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		compteEmetteur = compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		compteBeneficiaire = compteRepository.save(compte2);

		Virement v = new Virement();
		v.setMontantVirement(BigDecimal.TEN);
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotifVirement("Assignment 2021");

		virementSaved = virementRepository.save(v);

	}

	@Test
	public void findByIdVirementTest() {
		assertEquals(virementSaved, virementRepository.findById(virementSaved.getId()).get());
	}
	

	@Test
	public void saveVirementTest() {
		assertEquals("Assignment 2021", virementSaved.getMotifVirement());
		assertEquals(BigDecimal.TEN, virementSaved.getMontantVirement());
		assertEquals(compteEmetteur, virementSaved.getCompteEmetteur());
		assertEquals(compteBeneficiaire, virementSaved.getCompteBeneficiaire());
	}

	@Test
	public void deleteVirementTest() {
		Virement virement = virementRepository.findById(virementSaved.getId()).get();
		virementRepository.delete(virement);
		
		Virement virement1 = null;
		
		Optional<Virement> optionalVirement = virementRepository.findById(virement.getId());
		
		if(optionalVirement.isPresent()) {
			virement1 = optionalVirement.get();
		}
		
		Assertions.assertThat(virement1).isNull();
	}
}