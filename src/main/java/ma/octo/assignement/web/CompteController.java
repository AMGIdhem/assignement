package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.service.ICompteService;

@RestController
@RequestMapping(path = "/api")
public class CompteController {
	
	@Autowired
	ICompteService compteService;
	
	@GetMapping(path = "/comptes")
	public List<CompteDto> loadAllCompte() {
		return compteService.loadAllComptes();
	}
	
}
