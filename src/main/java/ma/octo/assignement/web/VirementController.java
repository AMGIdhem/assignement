package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVirementService;

@RestController
@RequestMapping(path = "/api")
class VirementController {

	@Autowired
	private IVirementService virementService;

	@GetMapping(path = "/virements")
	public List<VirementDto> loadAllVirements() {
		return virementService.loadAllVirements();
	}

	@PostMapping(path = "/virements")
	@ResponseStatus(HttpStatus.CREATED)
	public VirementDto createTransaction(@RequestBody VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		
		return virementService.createVirement(virementDto);
	}

}
