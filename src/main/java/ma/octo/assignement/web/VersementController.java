package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVersementService;

@RestController
@RequestMapping(path = "/api")
public class VersementController {

	@Autowired
	private IVersementService versementService;
	
	@GetMapping(path = "/versements") 
	public List<VersementDto> loadAllVersements() {
		return versementService.loadAllVersements();
	}

	@PostMapping(path = "/versements")
	@ResponseStatus(HttpStatus.CREATED)
	public VersementDto createTransaction(@RequestBody VersementDto versementDto) throws CompteNonExistantException, TransactionException {
		return versementService.createVersement(versementDto);
	}

}
