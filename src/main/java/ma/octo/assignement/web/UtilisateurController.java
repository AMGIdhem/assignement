package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.IUtilisateurService;

@RestController
@RequestMapping(path = "/api")
public class UtilisateurController {
	
	@Autowired
	IUtilisateurService utilisateurService;
	
	@GetMapping(path = "/utilisateurs")
	public List<UtilisateurDto> loadAllUtilisateur() {
		List<UtilisateurDto> utilisateurs = utilisateurService.loadAllUtilisateur();
		return utilisateurs;
	}

}
