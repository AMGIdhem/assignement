package ma.octo.assignement.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ROLE")
@Data @NoArgsConstructor @AllArgsConstructor //@EqualsAndHashCode
public class Role {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	private String name;
	
}
