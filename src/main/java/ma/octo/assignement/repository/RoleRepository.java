package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
