package ma.octo.assignement.mapper;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.repository.CompteRepository;

@Component
public class VersementMapper {

	@Autowired
	private CompteRepository compteRepository;

	public VersementDto mapFromModel(Versement versement) {
		VersementDto versementDto = new VersementDto();
		versementDto.setNomPrenomEmetteur(versement.getNomPrenomEmetteur());
		versementDto.setDateExecution(versement.getDateExecution());
		versementDto.setMotifVersement(versement.getMotifVersement());
		versementDto.setMontantVersement(versement.getMontantVersement());
		versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getNrCompte());

		return versementDto;

	}

	public Versement mapFromDto(VersementDto versementDTO) {
		Versement versement = new Versement();
		versement.setMontantVersement(versementDTO.getMontantVersement());
		versement.setDateExecution(new Date());
		versement.setNomPrenomEmetteur(versementDTO.getNomPrenomEmetteur());
		versement.setCompteBeneficiaire(compteRepository.findByNrCompte(versementDTO.getNrCompteBeneficiaire()));
		versement.setMotifVersement(versementDTO.getMotifVersement());

		return versement;
	}
}
