package ma.octo.assignement.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

@Component
public class CompteMapper {
	
	@Autowired
	private UtilisateurMapper utilisateurMapper;

	public CompteDto mapFromModel(Compte compte) {
		CompteDto compteDto = new CompteDto();
		compteDto.setNrCompte(compte.getNrCompte());
		compteDto.setRib(compte.getRib());
		compteDto.setSolde(compte.getSolde());
		compteDto.setUtilisateurDto(utilisateurMapper.mapFromModel(compte.getUtilisateur()));

		return compteDto;

	}

	public Compte mapFromDto(CompteDto compteDto) {
		Compte compte = new Compte();
		compte.setNrCompte(compteDto.getNrCompte());
		compte.setNrCompte(compteDto.getNrCompte());
		compte.setRib(compteDto.getRib());
		compte.setSolde(compteDto.getSolde());
		compte.setUtilisateur(utilisateurMapper.mapFromDto(compteDto.getUtilisateurDto()));

		return compte;
	}
}
