package ma.octo.assignement.mapper;

import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

@Component
public class UtilisateurMapper {

	public UtilisateurDto mapFromModel(Utilisateur utilisateur) {
		UtilisateurDto utilisateurDto = new UtilisateurDto();
		utilisateurDto.setFirstName(utilisateur.getFirstName());
		utilisateurDto.setLastName(utilisateur.getLastName());
		utilisateurDto.setUserName(utilisateur.getUserName());
		utilisateurDto.setGender(utilisateur.getGender());
		utilisateurDto.setBirthDate(utilisateur.getBirthDate());
		utilisateurDto.setEncryptedPassword(utilisateur.getEncryptedPassword());

		return utilisateurDto;

	}

	public Utilisateur mapFromDto(UtilisateurDto utilisateurDto) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setFirstName(utilisateurDto.getFirstName());
		utilisateur.setLastName(utilisateurDto.getLastName());
		utilisateur.setUserName(utilisateurDto.getUserName());
		utilisateur.setGender(utilisateurDto.getGender());
		utilisateur.setBirthDate(utilisateurDto.getBirthDate());

		return utilisateur;
	}
}
