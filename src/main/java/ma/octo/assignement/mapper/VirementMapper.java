package ma.octo.assignement.mapper;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;

@Component
public class VirementMapper {

	@Autowired
	private CompteRepository compteRepository;
	
    public VirementDto mapFromModel(Virement virement) {
    	VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());

        return virementDto;

    }
    
    public Virement mapFromDto(VirementDto virementDTO) {
    	Virement virement = new Virement();
    	virement.setMontantVirement(virementDTO.getMontantVirement());
    	virement.setDateExecution(new Date());
    	virement.setCompteEmetteur(compteRepository.findByNrCompte(virementDTO.getNrCompteEmetteur()));
    	virement.setCompteBeneficiaire(compteRepository.findByNrCompte(virementDTO.getNrCompteBeneficiaire()));
    	virement.setMotifVirement(virementDTO.getMotif());
    	
    	return virement;
    }
}
