package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.CompteDto;

public interface ICompteService {
	public List<CompteDto> loadAllComptes();
}
