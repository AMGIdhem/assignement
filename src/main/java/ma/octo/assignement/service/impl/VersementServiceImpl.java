package ma.octo.assignement.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVersementService;

@Service
@Transactional
public class VersementServiceImpl implements IVersementService {
	
	public static final int MONTANT_MAXIMAL = 10000;
	
	Logger LOGGER = LoggerFactory.getLogger(VersementServiceImpl.class);

	@Autowired
	private VersementRepository versementRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private IAuditService auditService;
	@Autowired
	private VersementMapper versementMapper; 
	
	
	@Override
	public List<VersementDto> loadAllVersements() {
		List<VersementDto> allVersementsDto = new LinkedList<>();
		List<Versement> allVersements = versementRepository.findAll();

		if (CollectionUtils.isEmpty(allVersements)) {
			return null;
		} else {
			for(Versement versement : allVersements) {
				allVersementsDto.add(versementMapper.mapFromModel(versement));
			}
			return allVersementsDto;
		}
	}

	@Override
	public VersementDto createVersement(VersementDto versementDto) 
			throws CompteNonExistantException, TransactionException {
		
		Compte compteBeneficiaire = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());
		

		if (compteBeneficiaire == null) {
			LOGGER.error("Compte Non existant");
			throw new CompteNonExistantException("Compte Non existant");
		}

		if (versementDto.getMontantVersement().equals(null)) {
			LOGGER.error("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (versementDto.getMontantVersement().intValue() == 0) {
			LOGGER.error("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (versementDto.getMontantVersement().intValue() < 10) {
			LOGGER.error("Montant minimal de versement non atteint");
			throw new TransactionException("Montant minimal de versement non atteint");
		} else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
			LOGGER.error("Montant maximal de versement dépassé");
			throw new TransactionException("Montant maximal de versement dépassé");
		}

		if (versementDto.getMotifVersement().isEmpty()) {
			LOGGER.error("Motif vide");
			throw new TransactionException("Motif vide");
		}

		compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
		compteRepository.save(compteBeneficiaire);

		Versement versement = versementMapper.mapFromDto(versementDto);

		Versement savedVersement = versementRepository.save(versement);

		auditService.auditVersement("Versement depuis " + savedVersement.getNomPrenomEmetteur() + " vers " + savedVersement
				.getCompteBeneficiaire().getNrCompte() + " d'un montant de " + savedVersement.getMontantVersement()
				.toString());
		
		return versementMapper.mapFromModel(savedVersement);
	}

}
