package ma.octo.assignement.service.impl;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;

@Service
@Transactional
public class CompteServiceImpl implements ICompteService {
	
	@Autowired
	CompteRepository compteRepository;
	@Autowired
	CompteMapper compteMapper;

	@Override
	public List<CompteDto> loadAllComptes() {
		LinkedList<CompteDto> allComptesDto = new LinkedList<>();
		List<Compte> allComptes = compteRepository.findAll();
		

		if (CollectionUtils.isEmpty(allComptes)) {
			return null;
		} else {
			for(Compte compte : allComptes) {
				allComptesDto.add(compteMapper.mapFromModel(compte));
			}
			return allComptesDto;
		}
	}

}
