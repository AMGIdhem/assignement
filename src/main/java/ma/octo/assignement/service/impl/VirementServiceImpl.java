package ma.octo.assignement.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVirementService;

@Service
@Transactional
public class VirementServiceImpl implements IVirementService{
	
	public static final int MONTANT_MAXIMAL = 10000;
	
	Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);
	
	@Autowired 
	CompteRepository compteRepository;
	@Autowired
	VirementRepository virementRepository;
	@Autowired
	IAuditService auditService;
	@Autowired
	VirementMapper virementMapper;

	@Override
	public List<VirementDto> loadAllVirements() {
		List<VirementDto> allVirementsDto = new LinkedList<>();
		List<Virement> allVirements = virementRepository.findAll();

		if (CollectionUtils.isEmpty(allVirements)) {
			return null;
		} else {
			for(Virement virement : allVirements) {
				allVirementsDto.add(virementMapper.mapFromModel(virement));
			}
			return allVirementsDto;
		}
	}
	
	@Override
	public VirementDto createVirement(VirementDto virementDto) throws CompteNonExistantException, TransactionException {
		Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
		Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

		if (compteEmetteur == null) {
			LOGGER.error("Compte Non existant");
			throw new CompteNonExistantException("Compte Non existant");
		}

		if (compteBeneficiaire == null) {
			LOGGER.error("Compte Non existant");
			throw new CompteNonExistantException("Compte Non existant");
		}

		if (virementDto.getMontantVirement().equals(null)) {
			LOGGER.error("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (virementDto.getMontantVirement().intValue() == 0) {
			LOGGER.error("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (virementDto.getMontantVirement().intValue() < 10) {
			LOGGER.error("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");
		} else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
			LOGGER.error("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");
		}

		if (virementDto.getMotif().length() < 0) {
			LOGGER.error("Motif vide");
			throw new TransactionException("Motif vide");
		}

		if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
			LOGGER.error("Solde insuffisant pour l'utilisateur");
		}

		if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
			LOGGER.error("Solde insuffisant pour l'utilisateur");
		}

		compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
		compteRepository.save(compteEmetteur);

		compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
		compteRepository.save(compteBeneficiaire);

		Virement virement = virementMapper.mapFromDto(virementDto);

		virementRepository.save(virement);

		auditService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
				.getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
				.toString());
		return virementMapper.mapFromModel(virement);
	}


}
