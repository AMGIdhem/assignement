package ma.octo.assignement.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;

@Service("utilisateurSeviceImpl")
@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService{
	
	@Autowired
	UtilisateurRepository utilisateurRepository;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	UtilisateurMapper utilisateurMapper;

	@Override
	public List<UtilisateurDto> loadAllUtilisateur() {
		List<UtilisateurDto> utilisateursDto = new LinkedList<>();
		List<Utilisateur> allUtilisateurs = utilisateurRepository.findAll();

		if (CollectionUtils.isEmpty(allUtilisateurs)) {
			return null;
		} else {
			for(Utilisateur utilisateur : allUtilisateurs) {
				utilisateursDto.add(utilisateurMapper.mapFromModel(utilisateur)); 
			}
			return utilisateursDto;
		}
	}
	
	@Override
	public Utilisateur getUtilisateur(String username) throws CompteNonExistantException {
		Utilisateur utilisateur = utilisateurRepository.findByUserName(username);
		if(utilisateur == null ) throw new CompteNonExistantException("no account with this id");
		return utilisateur;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Utilisateur utilisateur = utilisateurRepository.findByUserName(username);
		if(utilisateur == null) throw new UsernameNotFoundException(username);
		return new User(utilisateur.getUserName(), utilisateur.getEncryptedPassword(), getGrantedAuthorities(utilisateur));
	}
	
	private Collection<GrantedAuthority> getGrantedAuthorities(Utilisateur utilisateur) {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		if(utilisateur.getRole().getName().equalsIgnoreCase("admin")) {
			authorities.add(new SimpleGrantedAuthority("ROLE ADMIN"));
		}
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return authorities;
	}

}
