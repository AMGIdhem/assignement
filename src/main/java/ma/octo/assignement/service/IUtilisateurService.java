package ma.octo.assignement.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

public interface IUtilisateurService extends UserDetailsService {
	public Utilisateur getUtilisateur(String username) throws CompteNonExistantException;
	public List<UtilisateurDto> loadAllUtilisateur();
}
