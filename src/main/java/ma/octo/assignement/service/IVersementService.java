package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IVersementService {
	public VersementDto createVersement(VersementDto versementDto) 
			throws CompteNonExistantException, TransactionException;
	public List<VersementDto> loadAllVersements();
}
