package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IVirementService {
	public VirementDto createVirement(VirementDto virementDto) 
			throws CompteNonExistantException, TransactionException;
	public List<VirementDto> loadAllVirements();
}
