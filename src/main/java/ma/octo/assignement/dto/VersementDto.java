package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class VersementDto {
	
	private String nomPrenomEmetteur;
	private String nrCompteBeneficiaire;
	private String motifVersement;
	private BigDecimal montantVersement;
	private Date dateExecution;
	
}
