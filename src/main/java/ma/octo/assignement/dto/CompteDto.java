package ma.octo.assignement.dto;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "COMPTE")
@Data @NoArgsConstructor @AllArgsConstructor //@EqualsAndHashCode
public class CompteDto {

	private String nrCompte;
	private String rib;
	private BigDecimal solde;
	private UtilisateurDto utilisateurDto;

}
