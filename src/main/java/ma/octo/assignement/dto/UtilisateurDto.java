package ma.octo.assignement.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor //@EqualsAndHashCode
public class UtilisateurDto {

	private String userName;
	private String gender;
	private String lastName;
	private String firstName;
	private Date birthDate;
	private String encryptedPassword;
	
}
