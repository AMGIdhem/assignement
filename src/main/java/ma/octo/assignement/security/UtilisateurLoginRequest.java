package ma.octo.assignement.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UtilisateurLoginRequest {
	
	private String userName;
	private String password;

}
