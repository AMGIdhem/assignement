package ma.octo.assignement.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ma.octo.assignement.service.IUtilisateurService;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final IUtilisateurService utilisateurService;

	public WebSecurity(BCryptPasswordEncoder bCryptPasswordEncoder,IUtilisateurService utilisateurService) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.utilisateurService=utilisateurService;
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		/* Le code suivant desactive le filtre de Spring Security */

		http.authorizeRequests().antMatchers("/").permitAll()
								.antMatchers("/h2-console/**").permitAll();
		http.csrf().disable();
        http.headers().frameOptions().disable();
        
        
        
        /* Le code suivant pour activer le filtre de sécurité
        Login sur l'api : "/api/login" 
        Envoyer dans le body de la requete (userName : admin) et (password : 123)
        Recevoir le token comme réponse
        Copier le token et l'ajouter au header de la requete précédé par le prefix "Bearer" comme la forme suivante :
        KEY : Authorization		 VALUE : Bearer <token>
        Envoyer la requete au serveur */
        
//		http
//		.cors().and()
//		.csrf().disable()
//		.authorizeRequests()
//		.antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL)
//		.permitAll()
//		.anyRequest().authenticated()
//		.and()
//		.addFilter(getAuthenticationFilter())
//		.addFilter(new AuthorizationFilter(authenticationManager()))
//		.sessionManagement()
//		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        
	}



	protected AuthenticationFilter getAuthenticationFilter() throws Exception {
		final AuthenticationFilter filter = new AuthenticationFilter(authenticationManager());
		filter.setFilterProcessesUrl("/api/login");
		return filter;
	}


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(utilisateurService).passwordEncoder(bCryptPasswordEncoder);
	}


}
